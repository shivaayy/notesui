var noteList = {};
var Id = undefined;

hide();

noteData = function(heading, content) {
    this.heading = heading;
    this.content = content;
};

function saveNote(heading, content) {
    let unorderList = document.getElementById("noteslist");

    if (Id == undefined) {
        Id = Date()
        let child = document.createElement("LI");
        child.id = Id;
        child.innerHTML = heading;
        child.onclick = showNote;
        unorderList.appendChild(child);
    } else {
        document.getElementById(Id).innerHTML = heading;
    }

    noteList[Id] = new noteData(heading, content);

    alert("note saved successfully");
    hide();
}


document.getElementById("save").onclick = () => {
    saveNote(document.getElementById("heading").value, document.getElementById("content").value)
};


function deleteNote() {
    if (Id != undefined) {

        delete noteList[Id];
        document.getElementById(Id).remove();
    }

    hide();
    Id = undefined;
    alert("note deleted successfully");
}


function hide() {
    document.getElementById("heading").value = "";
    document.getElementById("content").value = "";
    document.getElementById("container").style.visibility = "hidden";
}


function showNote(event) {
    document.getElementById("container").style.visibility = "visible";
    document.getElementById("heading").value = noteList[event.target.id].heading;
    document.getElementById("content").value = noteList[event.target.id].content;
    Id = event.target.id;

}

document.getElementById("addnote").onclick = (c) => {
    Id = undefined;
    hide();
    document.getElementById("container").style.visibility = "visible";
};

document.getElementById("delete").onclick = deleteNote;